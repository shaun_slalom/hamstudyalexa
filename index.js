const Alexa = require(`alexa-sdk`);
const homeHandler = require(`./handlers/homeHandlers`);
const askingHandler = require(`./handlers/askQuestions`);
const quizOverHandler = require(`./handlers/quizOverHandlers`);
const emptyStateHandler = require(`./handlers/emptyStateHandlers`);
const helpHandlers = require(`./handlers/helpHandlers`);
const constants = require(`./constants/`);

exports.handler = (event, context) => {
  const alexa = Alexa.handler(event, context);
  alexa.dynamoDBTableName = `HAMStudy`;

  alexa.appId = constants.APP_ID;
  alexa.registerHandlers(
    homeHandler,
    askingHandler,
    quizOverHandler,
    emptyStateHandler,
    helpHandlers
  );
  alexa.execute();
};
