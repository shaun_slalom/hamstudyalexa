# Setup

## Install dependencies for development
---
`npm install`

## Run locally
---
`node index.js`
Just verify no syntax errors, testing requires upload to lambda.

## Build package for upload to AWS Lambda
---
`npm run build-aws-resource`

This will generate a zip file with the same name as the project folder that can be uploaded to your AWS lambda.

Login to the AWS and select (or create) the lambda you'd like to add the package/code to for execution:

https://console.aws.amazon.com/lambda/home#/functions

## Diagrams for test
![http://ncvec.org/downloads/T1.jpg](http://ncvec.org/downloads/T1.jpg "Image T1")

![http://ncvec.org/downloads/T2.jpg](http://ncvec.org/downloads/T2.jpg "Image T2")

![http://ncvec.org/downloads/T3.jpg](http://ncvec.org/downloads/T3.jpg "Image T3")


## Public skill in production

https://www.amazon.com/Chi-Town-Games-HAM-study/dp/B07BT9YRFS/ref=sr_1_1?s=digital-skills&ie=UTF8&qid=1523285054&sr=1-1&keywords=ham+study