const genericHandlers = require(`./genericHandlers`);
const Alexa = require(`alexa-sdk`);
const constants = require(`../constants`);
const prompts = require(`../prompts/genericPrompts`);

console.log(`creating help handler`);

module.exports = Alexa.CreateStateHandler(
  constants.states.help,
  Object.assign({}, genericHandlers, {
    'AMAZON.NoIntent': function NoIntent() {
      console.log(`Help - No Intent Handler`);
      this.handler.state = constants.states.home;
      this.attributes.STATE = constants.states.home;
      this.emit(`:tell`, prompts.Goodbye);
    },
    'AMAZON.YesIntent': function YesIntent() {
      console.log(`Help - Yes Intent Handler`);

      if (this.attributes.previousState === constants.states.asking &&
          this.attributes.currentQuestionIndex &&
          this.attributes.currentQuestionIndex > 0) {
        this.attributes.currentQuestionIndex--;
      }

      console.log(`Moving to state ${this.attributes.previousState}`);
      this.handler.state = this.attributes.previousState;
      this.emitWithState(`EnterState`);
    },
    'AMAZON.HelpIntent': function CancelIntent() {
      console.log(`Help - HelpIntent Handler`);
      this.emit(`:ask`, prompts.HelpIntent);
    }
  })
);
