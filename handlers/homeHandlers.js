const genericHandlers = require(`./genericHandlers`);
const Alexa = require(`alexa-sdk`);
const constants = require(`../constants`);
const questions = require(`../constants/QandA.json`);

console.log(`creating home handler`);

module.exports = Alexa.CreateStateHandler(
  constants.states.home,
  Object.assign({}, genericHandlers, {
    'AMAZON.NoIntent': function NoIntent() {
      console.log(`Home - No Intent Handler`);
      if (this.attributes.askedToResume) {
        this.attributes.resumeQuiz = false;
        this.handler.state = constants.states.asking;
        this.emitWithState(`EnterState`);
      } else {
        this.emit(`:tell`, `Have a good day`);
      }
    },
    'AMAZON.YesIntent': function YesIntent() {
      console.log(`Home - Yes Intent Handler`);
      if (this.attributes.askedToResume) {
        this.attributes.resumeQuiz = true;
        this.attributes.currentQuestionIndex--;
      } else {
        this.attributes.resumeQuiz = false;
      }

      console.log(`Moving to state ${constants.states.asking}`);
      this.handler.state = constants.states.asking;
      this.emitWithState(`EnterState`);
    },
    StartQuiz: function StartQuiz() {
      console.log(`Home - StartQuiz Handler`);
      this.handler.state = constants.states.asking;
      this.emitWithState(`EnterState`);
      console.log(`moving to state:`, this.handler.state);
    },
    LaunchRequest: function LaunchRequest() {
      console.log(`Home - LaunchRequest Handler`);
      this.emitWithState(`NewSession`);
    },
    EnterState: function LaunchRequest() {
      console.log(`Home - EnterState Handler`);
      this.emitWithState(`NewSession`);
    },
    NewSession: function NewSession() {
      console.log(`Home - NewSession Handler`);

      this.handler.state = constants.states.home;

      if (this.attributes.currentQuestionIndex && this.attributes.currentQuestionIndex < 35) {
        this.response.speak(`Welcome to HAM study. Would you like to resume your existing quiz?`).listen(`Would you like to resume your existing quiz?`);
        this.attributes.askedToResume = true;
        this.emit(`:responseReady`);
        return;
      }

      this.attributes.askedToResume = false;
      if (!this.attributes.gamesPlayed) { this.attributes.gamesPlayed = 0; }
      this.response.speak(`Welcome to HAM study. You have tested ${this.attributes.gamesPlayed.toString()} times. Would you like to start some study questions?`)
        .listen(`Say yes to start the test or no to quit.`);
      this.emit(`:responseReady`);
    }
  })
);
