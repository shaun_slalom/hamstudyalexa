const genericHandlers = require(`./genericHandlers`);
const Alexa = require(`alexa-sdk`);
const constants = require(`../constants`);

console.log(`creating home handler`);

module.exports = Alexa.CreateStateHandler(
  constants.states.endOfQuiz,
  Object.assign({}, genericHandlers, {
    QuizOver: function QuizOver() {
      console.log(`QuizOver - QuizOver Intent Handler`);

      const percentCorrect = parseInt((this.attributes.numCorrect / this.attributes.quizQuestions.length) * 100, 10);
      let response = `Your score is ${percentCorrect}% <break time="1s"/>`;
      if (percentCorrect > 75) {
        response += `Congratulations! You passed the test.`;
      } else {
        response += `Sorry to say your score is too low to pass this time, practice makes perfect`;
      }
      this.handler.state = constants.states.home;
      this.attributes.STATE = constants.states.home;
      if (!this.attributes.gamesPlayed) { this.attributes.gamesPlayed = 1; }
      this.attributes.gamesPlayed++;

      this.emit(`:tell`, response);
    }
  })
);
