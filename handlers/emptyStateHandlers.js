const genericHandlers = require(`./genericHandlers`);
const Alexa = require(`alexa-sdk`);

console.log(`creating empty state handler`);

module.exports = Alexa.CreateStateHandler(
  ``,
  Object.assign({}, genericHandlers)
);
