const prompts = require(`../prompts/genericPrompts`);
const constants = require(`../constants`);

module.exports = {
  // Switches to the state ther user has requested.
  'AMAZON.StopIntent': function StopIntent() {
    console.log(`Generic Stop Intent Handler`);
    this.emit(`:tell`, prompts.Canceled);
  },
  'AMAZON.CancelIntent': function CancelIntent() {
    console.log(`Generic Cancel Intent Handler`);
    this.emit(`:tell`, prompts.Canceled);
  },
  'AMAZON.HelpIntent': function CancelIntent() {
    this.attributes.previousState = this.attributes.STATE;
    this.handler.state = constants.states.help;

    this.emitWithState(`AMAZON.HelpIntent`);
  },
  SessionEndedRequest() {
    console.log(`Generic Session Ended Handler`);
    this.emit(`:saveState`, true);
  },
  StartQuiz: function StartQuiz() {
    this.handler.state = constants.states.home;
    this.emitWithState(`StartQuiz`);
  },
  Unhandled() {
    console.log(`Generic Unhandled Handler`);
    console.log(`Original event:`);
    console.log(this.event);
    this.emit(`:ask`, prompts.Unhandled);
  },
  LaunchRequest() {
    console.log(`Generic LaunchRequest Handler`);
    this.handler.state = constants.states.home;
    this.emitWithState(`LaunchRequest`);
  }
};
