const genericHandlers = require(`./genericHandlers`);
const Alexa = require(`alexa-sdk`);
const constants = require(`../constants`);
const questions = require(`../constants/QandA.json`);

console.log(`creating asking handler`);

function sayAnswer(answer) {
  return `<say-as interpret-as="spell-out">${answer.substr(0, 1)}</say-as> <break time="250ms"/>
          ${answer.substr(2)} <break time="500ms"/>`;
}

function moveToNextQuestion() { return this.attributes.currentQuestionIndex++; }

function combineQuestionsAndAnswers(selectedQuestion) {
  let combinedAnswers = `${selectedQuestion.question} <break time="1s"/>`;
  selectedQuestion.answers.map(anAnswer => combinedAnswers += sayAnswer(anAnswer));
  console.log(`All answers combined: ${combinedAnswers}`);
  return combinedAnswers;
}

function getNextQuestionAndAnswers() {
  const curQuestionIdx = moveToNextQuestion.call(this);
  console.log(`Current question index ${curQuestionIdx}`);
  const curQuestion = questions[this.attributes.quizQuestions[curQuestionIdx]];
  this.attributes.prevQuestion = this.attributes.quizQuestions[curQuestionIdx];
  const questionAndAnswers = combineQuestionsAndAnswers.call(this, curQuestion);
  return questionAndAnswers;
}

function initializeQuiz() {
  this.attributes.currentQuestionIndex = 0;
  this.attributes.quizQuestions = [];
  this.attributes.numCorrect = 0;
  // Populates the quizQuestions array with random unique questions.
  // Would be good to upgrade this to something that more intelligently picks questions
  // weighted based on how often it has been answered incorrectly and how often it has
  // been asked.
  const maxIndex = questions.length;

  // Building a pool of questions to pick out of, the pool allows me to easily
  // weight the chances of picking out questions that the examinee is doing
  // poorly at answering
  const questionPool = [];

  // Start off giving every question 1 chance of being picked
  for (let i = 0; i < maxIndex; i++) {
    questionPool.push(i);
  }

  if (Object.keys(this.attributes).length === 0 || !this.attributes.quizResults || this.attributes.quizResults.length == 0) {
    this.attributes.quizResults = [];
    for (let i = 0; i < questions.length; i++) {
      this.attributes.quizResults.push({
        timesAsked: 0,
        timesIncorrect: 0
      });
    }
  }

  for (let k = 0; k < this.attributes.quizResults.length; k++) {
    for (let l = 0; l < this.attributes.quizResults[k].timesIncorrect; l++) {
      questionPool.push(k);
    }
  }



  let j = 0;
  while (j < 35) {
    const randPosition = Math.floor(Math.random() * questionPool.length);
    const randQuestion = questionPool[randPosition];

    if (this.attributes.quizQuestions.indexOf(randQuestion) === -1) {
      this.attributes.quizQuestions.push(randQuestion);
      j++;
    }
  }
}

function getPreviousQuestionResponse() {
  const prevQuestion = questions[this.attributes.prevQuestion];
  return combineQuestionsAndAnswers.call(this, prevQuestion);
}

module.exports = Alexa.CreateStateHandler(
  constants.states.asking,
  Object.assign({}, genericHandlers, {
    EnterState() {
      console.log(`Entered the asking state`);
      if (!this.attributes.resumeQuiz) {
        initializeQuiz.call(this);
      }
      const questionAndAnswers = getNextQuestionAndAnswers.call(this);
      this.emit(`:ask`, questionAndAnswers, questionAndAnswers);
    },
    CheckScore() {
      if (this.attributes.currentQuestionIndex - 1 == 0) {
        let noAnswerResponse = `Your score is ${parseInt(this.attributes.numCorrect / (this.attributes.currentQuestionIndex - 1) * 100)}% <break time="1s"/>`;
        noAnswerResponse += getPreviousQuestionResponse.call(this);
        this.emit(`:ask`, noAnswerResponse, noAnswerResponse);
      }
      let response = `Your score is ${parseInt(this.attributes.numCorrect / (this.attributes.currentQuestionIndex - 1) * 100)}% <break time="1s"/>`;
      response += getPreviousQuestionResponse.call(this);
      this.emit(`:ask`, response, response);
    },
    CheckAnswer() {
      const prevQuestionIdx = this.attributes.prevQuestion;
      this.attributes.quizResults[prevQuestionIdx].timesAsked++;

      console.log(`CheckAnswer intent handler in ask questions`);

      const prevQuestion = questions[prevQuestionIdx];
      const previousCorrect = prevQuestion.correctAnswer;

      const intentObj = this.event.request.intent;
      if (!intentObj.slots.answer.resolutions.resolutionsPerAuthority[0].values) {
        this.emitWithState(`Unhandled`);
        return;
      }
      const usersAnswer = intentObj
        .slots
        .answer
        .resolutions
        .resolutionsPerAuthority[0]
        .values[0]
        .value
        .name;

      const answerToIndex = {
        A: 0,
        B: 1,
        C: 2,
        D: 3
      };
      console.log(`user answered: ${usersAnswer}`);

      if (this.attributes.currentQuestionIndex === this.attributes.quizQuestions.length) {
        if (usersAnswer.charAt(0).toLowerCase() === previousCorrect.charAt(0).toLowerCase()) {
          this.attributes.numCorrect++;
        } else {
          this.attributes.quizResults[prevQuestionIdx].timesIncorrect++;
        }
        this.handler.state = constants.states.endOfQuiz;
        this.emitWithState(`QuizOver`);
        return;
      }

      const questionAndAnswers = getNextQuestionAndAnswers.call(this);

      if (usersAnswer.charAt(0).toLowerCase() === previousCorrect.charAt(0).toLowerCase()) {
        this.attributes.numCorrect++;
        this.response.speak(`Great job!
          <break time="1s"/>Next Question. <break time="1s"/> ${questionAndAnswers}`)
          .listen(questionAndAnswers);
      } else {
        this.attributes.quizResults[prevQuestionIdx].timesIncorrect++;
        this.response.speak(`
          Sorry that answer is wrong.
          The correct answer is: ${sayAnswer(prevQuestion.answers[answerToIndex[previousCorrect]])}
          <break time="1s"/>Next Question. <break time="1s"/> ${questionAndAnswers}`)
          .listen(questionAndAnswers);
      }
      this.emit(`:responseReady`);
    },
    'AMAZON.YesIntent': function YesIntent() {
      console.log(`YES intent handler in ask questions`);
      this.emitWithState(`EnterState`);
    },
    'AMAZON.RepeatIntent': function RepeatIntent() {
      const response = getPreviousQuestionResponse.call(this);
      this.emit(`:ask`, response, response);
    },
    'AMAZON.StartOverIntent': function StartOverIntent() {
      initializeQuiz.call(this);
      this.handler.state = constants.states.home;
      this.emitWithState(`EnterState`);
    },
    QuizOver: function QuizOver() {
      console.log(`QuizOver - QuizOver Intent Handler`);
      this.handler.state = constants.states.endOfQuiz;
      this.attributes.STATE = constants.states.endOfQuiz;
      this.emitWithState(`QuizOver`);
    }
  })
);
