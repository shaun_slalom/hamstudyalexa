module.exports = {
  APP_ID: `amzn1.ask.skill.837237f6-f95e-44ab-b805-e460ef365132`,
  states: {
    home: `HOME`,
    help: `HELP`,
    asking: `ASKING_QUESTION`,
    endOfQuiz: `END_OF_QUIZ`
  }
};

