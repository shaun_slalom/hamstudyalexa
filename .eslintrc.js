module.exports = {
    "extends": "airbnb-base",
    "rules": {
        "prefer-template": 1,
        "quotes": ["warn", "backtick"],
        "no-console": 0,
        "comma-dangle": 0
    }
};